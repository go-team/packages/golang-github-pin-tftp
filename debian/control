Source: golang-github-pin-tftp
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Dominik George <natureshadow@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-golang-x-net-dev,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-pin-tftp
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-pin-tftp.git
Homepage: https://github.com/pin/tftp
XS-Go-Import-Path: github.com/pin/tftp

Package: golang-github-pin-tftp-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-golang-x-net-dev,
         ${misc:Depends},
Description: TFTP server and client library for Golang (library)
 This is a library helping to implement clients and servers for the
 TFTP protocol in Go.
 .
 It implements:
 .
  * RFC 1350 - The TFTP Protocol (Revision 2)
  * RFC 2347 - TFTP Option Extension
  * RFC 2348 - TFTP Blocksize Option
 .
 It partially implements (server side only):
 .
  * RFC 2349 - TFTP Timeout Interval and Transfer Size Options
 .
 The set of features is sufficient for PXE boot support.
